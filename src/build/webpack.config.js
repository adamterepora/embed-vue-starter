const path = require( 'path' );
const webpack = require( 'webpack' );
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );

function resolve (dir) {
    return path.join(__dirname, '..', dir)
}

module.exports = function( env = {} ) {
    if ( env.production )
        process.env.NODE_ENV = 'production';

    if ( process.env.NODE_ENV == 'production' )
        __webpack_public_path__ = 'http://example.com/assets/';

    function makeStyleLoader( type ) {
        const cssLoader = {
            loader: 'css-loader',
            options: {
                minimize: env.production
            }
        };
        const loaders = [ cssLoader ];
        if ( type )
            loaders.push( type + '-loader' );
        if ( env.production ) {
            return ExtractTextPlugin.extract( {
                use: loaders,
                fallback: 'vue-style-loader'
            } );
        } else {
            return [ 'vue-style-loader' ].concat( loaders );
        }
    }

    return {
        entry: './src/main.js',
        output: {
            path: path.resolve( __dirname, '../../assets' ),
            filename: env.production ? 'js/main.min.js' : 'js/main.js',
            publicPath: env.production ? '../' : 'http://localhost:8081/'
        },
        plugins: env.production ? [
            new webpack.DefinePlugin( {
                'process.env': {
                    NODE_ENV: '"production"'
                }
            } ),
            new webpack.optimize.UglifyJsPlugin( {
                compress: {
                    warnings: false
                }
            } ),
            new ExtractTextPlugin( {
                filename: 'css/style.min.css'
            } )
        ] : [
            new webpack.HotModuleReplacementPlugin()
        ],
        devServer: {
            contentBase: false,
            hot: true,
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        },
        devtool: env.production ? false : '#cheap-module-eval-source-map',
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            css: makeStyleLoader()
                            // less: makeStyleLoader( 'less' )
                        }
                    }
                },
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.(png|jpg)$/,
                    loader: 'file-loader',
                    options: {
                        name: 'images/[name].[ext]'
                    }
                },
                {
                    test: /\.css$/,
                    use: makeStyleLoader()
                }
                // {
                //     test: /\.sass$/,
                //     use: makeStyleLoader( 'sass' )
                // }
            ]
        },
        resolve: {
            extensions: [ '.js', '.vue', '.json' ],
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
                '@': path.resolve( __dirname, '..' )
            }
        }
    };
};
