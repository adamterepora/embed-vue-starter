This is the starter pack that allows to embed and livereload package in php files.
All you need to do is clone package into your php project, then run:
```
npm install && npm run dev
```

and in php embed your script:
```
<div id="application" data-url="someUrl" data-id="someId">
    <div id="app"></div>
</div>
<script src="http://localhost:8081/js/main.js"></script>
```